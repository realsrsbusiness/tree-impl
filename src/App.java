package com.fdmgroup.app;

import java.util.Stack;

public class App 
{
    public static void main(String[] args)
    {
        NumberTree nt = new NumberTree();

        TreeNode<Integer> t = new TreeNode<Integer>();

        nt.add(15);
        nt.add(10);
        nt.add(20);
        nt.add(5);

        nt.add(1);
        nt.add(22);

        nt.print();

        System.out.println();
    }

    public static class NumberTree {
        TreeNode<Integer> root = new TreeNode<Integer>();

        public void add(int value) {
            var current = root;

            while(true) {
                if(current.payload == null) {
                    current.payload = value;
                    break;
                }
                    
                if(value >= current.payload) {
                    if(current.right == null) {
                        current.right = new TreeNode<>();
                        current.right.payload = value;

                        break;
                    } else {
                        current = current.right;
                    }

                } else {
                    if(current.left == null) {
                        current.left = new TreeNode<>();
                        current.left.payload = value;

                        break;
                    } else {
                        current = current.left;
                    }
                }
            }

        }

        public void print() {
            Stack<TreeNode<Integer>> toObserve = new Stack<>();
            toObserve.add(root);

            int level = 0;

            do {
                var val = toObserve.pop();
                level = val.level;
                
                for (int i = 0; i < val.level; i++) {
                    System.out.print(" ");
                }
                System.out.println(val.payload);

                if(val.left != null || val.right != null)
                    level++;

                if(val.left != null) {
                    val.left.level = level;
                    toObserve.push(val.left);
                }
                    
                if(val.right != null) {
                    val.right.level = level;
                    toObserve.push(val.right);
                }

            } while(toObserve.size() > 0);

        }
    }

    public static class TreeNode<T> {
        public TreeNode<T> left, right;
        T payload;

        public int level = 0;

        @Override
        public String toString() {
            return payload.toString();
        }
    }

}
